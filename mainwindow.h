/*
 * SPDX-FileCopyrightText: 2023 Bruno Anselme <be.root@gfree.fr>
 *
 * Authors:
 *   Bruno Anselme <be.root@gfree.fr>
 *
 *   SPDX-License-Identifier: GPL-3.0-or-later
 */
/* mainwindow.h  -- Main window for Shuttimer */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QTimer>
#include <QApplication>
#include <QMainWindow>
#include <QSoundEffect>
#include <QtDBus/QtDBus>
#include <QDBusInterface>
#include <QSettings>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    enum Actions { BELL, LOCK, LOGOUT, SUSPEND, HIBERNATE
                   , SESSION_SHUTDOWN, SESSION_REBOOT
                   , SYSTEM_SHUTDOWN, SYSTEM_REBOOT
                   , HARD_POWEROFF, HARD_REBOOT };

    void setTimeout(QTime);
    void setAction(Actions);

private:
    QString execute(QString program, QStringList &arguments);
    QString getSessionId();
    void restoreSettings();
    void saveSettings();

protected:
    void closeEvent(QCloseEvent *event);

public Q_SLOTS:
    void startIt();
    void stopIt();
    void finished();
    void refreshCounter();
    void actionChanged(int);
    void about();
    void timeValueChanged();

private:
    Ui::MainWindow *ui;
    QTimer actionTimer;
    QTimer countDown;
    quint64 delay;
    quint64 remain;
    QSoundEffect bell;
    QString loginCtl;
    QString systemCtl;
    QString dbusSend;
    QString sessionId;      // required for loginctl lock-session
    QString timeFormat;
    QSettings settings;
};
#endif // MAINWINDOW_H
