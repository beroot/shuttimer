%ifarch x86_64
%define _bit %(getconf LONG_BIT)
%endif

Name:    shuttimer
Version: 0.22.0
Release: %mkrel 1
Summary: Shutdown timer
License: GPLv3+
Group:   System tools
Url:     http://be.root.free.fr
Source0: http://be.root.free.fr/soft/%{name}/%{name}-%{version}.tar.gz

%description
Shutdown timer for Linux.

%prep
%setup -q

%build
%qmake_qt5
%__make

%install
%make_install INSTALL_ROOT=%{buildroot}

# Files installed by make install need to be removed. Give a list here.
%files
%{_bindir}/%{name}
%{_datadir}/icons/%{name}*.png
%{_datadir}/applications/%{name}.desktop

%changelog
* Fri Jan 05 2024 Bruno Anselme<be.root@free.fr> 0.20.0
- Updating for git.

