#-------------------------------------------------
# Project created by QtCreator 2023-10-23
# Shutdown scheduler using dbus and/or system calls.
# 2023-2024 Bruno ANSELME <be.root@free.fr>
#-------------------------------------------------

QT += core gui
QT += dbus
QT += multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET      = shuttimer
TEMPLATE    = app
MAJOR       = 0
MINOR       = 23
RELEASE     = 0
VERSION     = $${MAJOR}.$${MINOR}.$${RELEASE}

CONFIG += c++17
CONFIG += console

DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h \
    app_version.h \
    myspinbox.h

FORMS += mainwindow.ui

RESOURCES += \
  shuttimer.qrc

TRANSLATIONS += \
    shuttimer_fr_FR.ts
CONFIG += lrelease
CONFIG += embed_translations

OTHER_FILES += \
    $${TARGET}.desktop $${TARGET}.png \
    *.qm   \                                # Translations
    $${TARGET}.spec \
    *.wav \
    shuttimer-1.png \
    license-gpl-3.0.* \                     # License
    README.md \
    $${TARGET}.pro *.pri                    # QtCreator project

target.path     = /usr/bin                  # Application installation dir

icons.path      = /usr/share/icons
icons.files     = $${TARGET}*.png

desktop.path    = /usr/share/applications
desktop.files   = $${TARGET}.desktop

INSTALLS += icons desktop

# More make targets
include(more-make-targets.pri)              # Additional targets : version, translation, files, count
QMAKE_EXTRA_TARGETS += help                 # In case of mulitple .pri, each file add help lines. Register help target when finished.

# Default rules for deployment.
!isEmpty(target.path): INSTALLS += target
