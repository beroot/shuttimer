<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="83"/>
        <source>Bell</source>
        <translation>Sonnerie</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="88"/>
        <source>Lock session</source>
        <translation>Verrouiller la session</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="93"/>
        <source>Logout</source>
        <translation>Déconnexion</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <source>Suspend</source>
        <translation>Mettre en veille</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="108"/>
        <source>Logout and shutdown</source>
        <oldsource>Shutdown</oldsource>
        <translation>Déconnexion et extinction</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="118"/>
        <source>DBus shutdown</source>
        <translation>Arrêt par DBus</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="123"/>
        <source>DBus reboot</source>
        <translation>Redémarrage par DBus</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="128"/>
        <source>Hard shutdown</source>
        <translation>Arrêt brutal (shutdown)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="133"/>
        <source>Hard reboot</source>
        <translation>Redémarrage brutal (reboot)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="213"/>
        <source>Timeout</source>
        <translation>Délai d&apos;attente</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="232"/>
        <source> h</source>
        <translation> h</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="254"/>
        <source> m</source>
        <translation> m</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="279"/>
        <source> s</source>
        <translation> s</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="322"/>
        <source>End time</source>
        <translation>Heure de fin</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="160"/>
        <source>Loop bell</source>
        <translation>Répéter la sonnerie</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="103"/>
        <source>Hibernate</source>
        <translation>Mettre en hibernation</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="61"/>
        <location filename="mainwindow.cpp" line="176"/>
        <source>Start</source>
        <translation>Démarrer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="113"/>
        <source>Logout and reboot</source>
        <translation>Déconnexion et redémarrage</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="149"/>
        <source>Stop</source>
        <translation>Arrêter</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source>Timer is running.
Do you really want to quit?</source>
        <oldsource>Timer is running.
Do you really want to quit?
</oldsource>
        <translation>La minuterie tourne.
Voulez-vous vraiment quitter ?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="81"/>
        <source>About Shuttimer</source>
        <translation>À propos de Shuttimer</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="81"/>
        <source>&lt;b&gt;Shuttimer&lt;/b&gt; is a shutdown timer for Unix/KDE.&lt;br&gt;Written by Bruno ANSELME in 2023 with Qt.&lt;br&gt;&lt;u&gt;Email&lt;/u&gt;: be.root@free.fr.&lt;br&gt;&lt;br&gt;License: GPL-3.0-or-later&lt;br&gt;Bell sound: Mixkit Sound Effects Free License&lt;br&gt;https://mixkit.co/free-sound-effects/&lt;br&gt;&lt;br&gt;Command line options are available:&lt;br&gt;  Enter &lt;i&gt;shuttimer --help&lt;/i&gt; from terminal.</source>
        <translation>&lt;b&gt;Shuttimer&lt;/b&gt; est un minuteur d&apos;extinction pour Unix/KDE.&lt;br&gt;Écrit par Bruno ANSELME en 2023 avec Qt.&lt;br&gt;&lt;u&gt;Email&lt;/u&gt; : be.root@free.fr.&lt;br&gt;&lt;br&gt;Licence : GPL-3.0-or-later&lt;br&gt;Sonnerie : Mixkit Sound Effects Free License&lt;br&gt;https://mixkit.co/free-sound-effects/&lt;br&gt;&lt;br&gt;Des options en ligne de commande sont disponibles :&lt;br&gt;  Entrer &lt;i&gt;shuttimer --help&lt;/i&gt; depuis un terminal.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="141"/>
        <source>in</source>
        <oldsource>at</oldsource>
        <translation>dans</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="149"/>
        <source>Continue</source>
        <translation>Continuer</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="153"/>
        <location filename="mainwindow.cpp" line="166"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="178"/>
        <source>Finished at</source>
        <translation>Terminé à</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="31"/>
        <location filename="mainwindow.cpp" line="180"/>
        <source>Shutdown timer</source>
        <translation>Minuteur d&apos;extinction</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="66"/>
        <source>Shutdown timer.</source>
        <translation>Minuteur d&apos;extinction.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="68"/>
        <source>Show author informations.</source>
        <translation>Afficher les informations sur l&apos;auteur.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="70"/>
        <source>Show license informations.</source>
        <translation>Afficher les informations de licence.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="72"/>
        <source>Timer delay: HH:MM:SS.</source>
        <translation>Délai du minuteur: HH:MM:SS.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="74"/>
        <source>Actions: %1</source>
        <translation>Actions: %1</translation>
    </message>
    <message>
        <location filename="main.cpp" line="76"/>
        <source>Start timer now.</source>
        <translation>Démarrer le minuteur tout de suite.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="84"/>
        <source>%1 was written by:
	%2 &lt;%3&gt;
</source>
        <translation>%1 a été écrit par :
	%2 &lt;%3&gt;
</translation>
    </message>
    <message>
        <location filename="main.cpp" line="85"/>
        <source>	Web site : %1
</source>
        <translation>	Site web : %1
</translation>
    </message>
    <message>
        <location filename="main.cpp" line="102"/>
        <source>is not a valid time.</source>
        <translation>n&apos;est pas une durée valide.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="112"/>
        <source>is not a valid action.</source>
        <translation>n&apos;est pas une action valide.</translation>
    </message>
</context>
</TS>
