/*
 * SPDX-FileCopyrightText: 2023 Bruno Anselme <be.root@gfree.fr>
 *
 * Authors:
 *   Bruno Anselme <be.root@gfree.fr>
 *
 *   SPDX-License-Identifier: GPL-3.0-or-later
 */
/* myspinbox.h  -- Include file for Shuttimer */
#ifndef MYSPINBOX_H
#define MYSPINBOX_H
#include <QSpinBox>

class MySpinBox: public QSpinBox
{
    Q_OBJECT

public:
    MySpinBox( QWidget * parent = 0) : QSpinBox(parent) {}

    virtual QString textFromValue ( int value ) const {
        return QString("%1").arg(value, 2 , 10, QChar('0'));    // 2  digits, base 10, pad chars to '0'
    }
};

#endif // MYSPINBOX_H
