/*
 * SPDX-FileCopyrightText: 2023 Bruno Anselme <be.root@gfree.fr>
 *
 * Authors:
 *   Bruno Anselme <be.root@gfree.fr>
 *
 *   SPDX-License-Identifier: GPL-3.0-or-later
 */
/* main.cpp  -- Main C++ program for Shuttimer */

#include <iostream>

#include <QApplication>
#include <QLocale>
#include <QTranslator>
#include <QCommandLineParser>

#include "mainwindow.h"
#include "app_version.h"

// File functions
QString readFile(QString fname) {
    QFile file(fname);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return QString();
    QTextStream f(&file);
    QString txt = f.readAll();
    file.close();
    return txt;
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setOrganizationName(APP_ORG);
    app.setOrganizationDomain(APP_MAIL);
    app.setApplicationName(APP_NAME);
    app.setApplicationVersion(APP_VERSION);

    // Actions map to parse command line options
    QMap<QString, MainWindow::Actions> mapActions;
    mapActions["BELL"]              = MainWindow::Actions::BELL;
    mapActions["LOCK"]              = MainWindow::Actions::LOCK;
    mapActions["LOGOUT"]            = MainWindow::Actions::LOGOUT;
    mapActions["SUSPEND"]           = MainWindow::Actions::SUSPEND;
    mapActions["HIBERNATE"]         = MainWindow::Actions::HIBERNATE;
    mapActions["SESSION_SHUTDOWN"]  = MainWindow::Actions::SESSION_SHUTDOWN;
    mapActions["SESSION_REBOOT"]    = MainWindow::Actions::SESSION_REBOOT;
    mapActions["SYSTEM_SHUTDOWN"]   = MainWindow::Actions::SYSTEM_SHUTDOWN;
    mapActions["SYSTEM_REBOOT"]     = MainWindow::Actions::SYSTEM_REBOOT;
    mapActions["HARD_POWEROFF"]     = MainWindow::Actions::HARD_POWEROFF;
    mapActions["HARD_REBOOT"]       = MainWindow::Actions::HARD_REBOOT;

    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "shuttimer_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            app.installTranslator(&translator);
            break;
        }
    }

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.setApplicationDescription(QObject::tr("Shutdown timer."));
    parser.addVersionOption();
    QCommandLineOption authorOption(QStringList()  << "A" << "author",      QObject::tr("Show author informations."));
    parser.addOption(authorOption);
    QCommandLineOption licenseOption(QStringList() << "i" << "license",     QObject::tr("Show license informations."));
    parser.addOption(licenseOption);
    QCommandLineOption delayOption(QStringList() << "d" << "delay",         QObject::tr("Timer delay: HH:MM:SS."), "delay");
    parser.addOption(delayOption);
    QCommandLineOption actionOption(QStringList() << "a" << "action",       QObject::tr("Actions: %1").arg("BELL, LOCK, LOGOUT, SUSPEND, HIBERNATE, SESSION_SHUTDOWN, SESSION_REBOOT, SYSTEM_SHUTDOWN, SYSTEM_REBOOT, HARD_POWEROFF, HARD_REBOOT."), "action");
    parser.addOption(actionOption);
    QCommandLineOption startOption(QStringList()  << "s" << "start",        QObject::tr("Start timer now."));
    parser.addOption(startOption);

    // Process the actual command line arguments given by the user
    parser.process(app);

    // Test for some command line options. Run and exit
    if (parser.isSet("author")) {
        std::cout << QObject::tr("%1 was written by:\n\t%2 <%3>\n").arg(APP_STR).arg(APP_AUTHOR).arg(APP_MAIL).toStdString().c_str();
        std::cout << QObject::tr("\tWeb site : %1\n").arg(APP_SITE).toStdString().c_str();
        exit(EXIT_SUCCESS);
    }
    if (parser.isSet("license")) {
        std::cout << readFile(":/license-gpl-3.0.txt").toStdString().c_str();
        exit(EXIT_SUCCESS);
    }

    MainWindow w;

    // Other command line options
    if (parser.isSet("delay")) {
        QTime delay = QTime::fromString(parser.value(delayOption), Qt::ISODate);
        if (delay.isValid()) {
            w.setTimeout(delay);
        } else {
            std::cerr << parser.value(delayOption).toStdString().c_str() << " " <<
                         QObject::tr("is not a valid time.").toStdString().c_str() << "\n";
            exit(EXIT_SUCCESS);
        }
    }
    if (parser.isSet("action")) {
        QString actionText = parser.value(actionOption).toUpper();
        if (mapActions.contains(actionText)) {
            w.setAction(mapActions[actionText]);
        } else {
            std::cerr << actionText.toStdString().c_str() << " " <<
                         QObject::tr("is not a valid action.").toStdString().c_str() << "\n";
        }
    }

    w.show();
    if (parser.isSet("start")) {
        w.startIt();
    }

    return app.exec();
}
