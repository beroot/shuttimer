/*
 * SPDX-FileCopyrightText: 2023 Bruno Anselme <be.root@gfree.fr>
 *
 * Authors:
 *   Bruno Anselme <be.root@gfree.fr>
 *
 *   SPDX-License-Identifier: GPL-3.0-or-later
 */
/* mainwindow.cpp  -- Main window for Shuttimer */

#include <QCloseEvent>
#include <QMessageBox>

#include "mainwindow.h"
#include "app_version.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , actionTimer(parent)
    , countDown(parent)
    , delay(0)
    , remain(0)
    , bell()
    , loginCtl("/usr/bin/loginctl")
    , systemCtl("/bin/systemctl")
    , dbusSend("/usr/bin/dbus-send")
    , sessionId("")
    , timeFormat("hh:mm:ss")
    , settings(APP_ORG, APP_NAME)
{
    ui->setupUi(this);

    bell.setSource(QUrl::fromLocalFile(":/mixkit-fantasy-bells-582.wav"));
    countDown.setSingleShot(false);
    ui->remainingTime->setText(QTime::fromMSecsSinceStartOfDay(0).toString(timeFormat));

    connect(ui->startButton, SIGNAL(pressed()), this, SLOT(startIt()));
    connect(ui->stopButton, SIGNAL(pressed()), this, SLOT(stopIt()));
    connect(&actionTimer, SIGNAL(timeout()), this, SLOT(finished()));
    connect(&countDown, SIGNAL(timeout()), this, SLOT(refreshCounter()));
    connect(ui->wantedAction, SIGNAL(currentIndexChanged(int)), this, SLOT(actionChanged(int)));
    connect(ui->aboutButton, SIGNAL(clicked()), this, SLOT(about()));

    connect(ui->spinHour, SIGNAL(valueChanged(int)), this, SLOT(timeValueChanged()));
    connect(ui->spinMinute, SIGNAL(valueChanged(int)), this, SLOT(timeValueChanged()));
    connect(ui->spinSecond, SIGNAL(valueChanged(int)), this, SLOT(timeValueChanged()));

    sessionId = getSessionId();

    restoreSettings();
    countDown.setInterval(1000);
    countDown.start();
}

MainWindow::~MainWindow()
{
    saveSettings();
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event) {
    if (actionTimer.isActive()) {
        QMessageBox::StandardButton resBtn = QMessageBox::question(
                    this, APP_NAME,
                    tr("Timer is running.\nDo you really want to quit?"),
                    QMessageBox::No | QMessageBox::Yes,
                    QMessageBox::No);
        if (resBtn != QMessageBox::Yes) {
            event->ignore();
        } else {
            event->accept();
        }
    } else {
        event->accept();
    }
}

void MainWindow::about() {
    QMessageBox::about(this, tr("About Shuttimer"), tr("<b>Shuttimer</b> is a shutdown timer for Unix/KDE.<br>\
Written by Bruno ANSELME in 2023 with Qt.<br>\
<u>Email</u>: be.root@free.fr.<br><br>\
License: GPL-3.0-or-later<br>\
Bell sound: Mixkit Sound Effects Free License<br>https://mixkit.co/free-sound-effects/<br><br>\
Command line options are available:<br>  Enter <i>shuttimer --help</i> from terminal."));
}

void MainWindow::timeValueChanged() {
    QDateTime startTime = QDateTime::currentDateTime();
    QDateTime stopTime = startTime.addSecs(ui->spinHour->value() * 3600 + ui->spinMinute->value() * 60 + ui->spinSecond->value());
    ui->endDate->setText(stopTime.toString(timeFormat));
}

void MainWindow::setTimeout(QTime delay) {
    ui->spinHour->setValue(delay.hour());
    ui->spinMinute->setValue(delay.minute());
    ui->spinSecond->setValue(delay.second());
}

void MainWindow::setAction(Actions what) {
    ui->wantedAction->setCurrentIndex(what);
}

void MainWindow::restoreSettings() {
    move(settings.value("position", QPoint(100, 100)).toPoint());
    resize(settings.value("size",   QSize(454, 519)).toSize());
    ui->spinHour->setValue(settings.value("hour", 0).toInt());
    ui->spinMinute->setValue(settings.value("minute", 0).toInt());
    ui->spinSecond->setValue(settings.value("second", 15).toInt());
    ui->wantedAction->setCurrentIndex(settings.value("action", 0).toInt());
    ui->scheduledDateTime->setText(QString("<i>%1</i>").arg(settings.value("message", "").toString()));
}

void MainWindow::saveSettings() {
    settings.setValue("position", pos());
    settings.setValue("size",     size());
    settings.setValue("hour",    ui->spinHour->value());
    settings.setValue("minute",  ui->spinMinute->value());
    settings.setValue("second",  ui->spinSecond->value());
    settings.setValue("action",  ui->wantedAction->currentIndex());
    settings.setValue("message", ui->scheduledDateTime->text());
}

void MainWindow::actionChanged(int index) {
    ui->loopBell->setEnabled(index == 0);
}

void MainWindow::refreshCounter() {
    QTime remaining(0, 0, 0);
    if (actionTimer.isActive()) {
        remaining = remaining.addMSecs(actionTimer.remainingTime());
    } else {
        remaining = remaining.addMSecs(remain);
        timeValueChanged();
    }
    ui->remainingTime->setText(remaining.toString(timeFormat));
    setWindowTitle(ui->wantedAction->currentText() + " " + tr("in") + " " + remaining.toString(timeFormat));
}

void MainWindow::startIt() {
    bell.stop();
    if (ui->startButton->isChecked()) {
        remain = actionTimer.remainingTime();
        actionTimer.stop();
        ui->startButton->setText(tr("Continue"));
        return;
    } else if (remain > 0) {
        actionTimer.start(remain);
        ui->startButton->setText(tr("Pause"));
        return;
    }

    QDateTime startTime = QDateTime::currentDateTime();
    QDateTime stopTime = startTime.addSecs(ui->spinHour->value() * 3600 + ui->spinMinute->value() * 60 + ui->spinSecond->value());
    delay = startTime.secsTo(stopTime);
    actionTimer.setInterval(delay * 1000);
    actionTimer.start();
    refreshCounter();
    ui->stopButton->setEnabled(true);
    ui->startButton->setText(tr("Pause"));
    ui->spinHour->setEnabled(false);
    ui->spinMinute->setEnabled(false);
    ui->spinSecond->setEnabled(false);
    ui->wantedAction->setEnabled(false);
}

void MainWindow::stopIt() {
    ui->stopButton->setEnabled(false);
    ui->startButton->setText(tr("Start"));
    ui->startButton->setChecked(false);
    ui->scheduledDateTime->setText(tr("Finished at") + " " + QDateTime::currentDateTime().toString(timeFormat));
    ui->remainingTime->setText(QTime::fromMSecsSinceStartOfDay(0).toString(timeFormat));
    setWindowTitle(tr("Shutdown timer"));
    actionTimer.stop();
    bell.stop();
    remain = 0;
    ui->spinHour->setEnabled(true);
    ui->spinMinute->setEnabled(true);
    ui->spinSecond->setEnabled(true);
    ui->wantedAction->setEnabled(true);
}

QString MainWindow::getSessionId() {
    QString id("");
    QStringList args = { "list-sessions" };
    QString out = execute(loginCtl, args);
    QRegularExpression rx(".*\\n\\s*(\\w+\\d+).*");
    QRegularExpressionMatch match = rx.match(out);
    if (match.hasMatch()) {
        id = match.captured(1);
    }
    return id;
}

QString MainWindow::execute(QString program, QStringList &arguments) {
    QProcess *myProcess = new QProcess(this);
    myProcess->start(program, arguments);
    myProcess->waitForFinished();
    return myProcess->readAllStandardOutput();
}

void MainWindow::finished() {
    show();                                         // Restore from systray
    setWindowState(Qt::WindowState::WindowActive);  // Bring window to foreground
    stopIt();
    saveSettings();
    switch (ui->wantedAction->currentIndex()) {
    case Actions::BELL :
        ui->stopButton->setEnabled(true);           // to stop looping sound
        bell.setLoopCount(ui->loopBell->isChecked() ? QSoundEffect::Infinite : 0);
        bell.play();
        break;
    case Actions::LOCK : {                          // systemd
        QStringList args = { "lock-session", sessionId };
        execute(loginCtl, args);
        break;
    }
    case Actions::LOGOUT : {                        // dbus session
        QStringList args = { "--session", "--print-reply", "--dest=org.kde.Shutdown",
                             "/Shutdown", "org.kde.Shutdown.logout",
                           };
        execute(dbusSend, args);
        break;
    }
    case Actions::SUSPEND : {                       // systemd
        QStringList args = { "suspend" };
        execute(systemCtl, args);
        break;
    }
    case Actions::HIBERNATE :{                      // systemd
        QStringList args = { "hibernate" };
        execute(systemCtl, args);
        break;
    }
    case Actions::SESSION_SHUTDOWN : {              // dbus user session
        QStringList args = { "--session", "--print-reply", "--dest=org.kde.Shutdown",
                             "/Shutdown", "org.kde.Shutdown.logoutAndShutdown",
                           };
        execute(dbusSend, args);
        break;
    }
    case Actions::SESSION_REBOOT : {                // dbus user session
        QStringList args = { "--session", "--print-reply", "--dest=org.kde.Shutdown",
                             "/Shutdown", "org.kde.Shutdown.logoutAndReboot",
                           };
        execute(dbusSend, args);
        break;
    }
    case Actions::SYSTEM_SHUTDOWN : {               // dbus system
        QStringList args = { "--system", "--print-reply", "--dest=org.freedesktop.login1",
                             "/org/freedesktop/login1", "org.freedesktop.login1.Manager.PowerOff",
                             "boolean:true",
                           };
        execute(dbusSend, args);
        break;
    }
    case Actions::SYSTEM_REBOOT : {                 // dbus system
        QStringList args = { "--system", "--print-reply", "--dest=org.freedesktop.login1",
                             "/org/freedesktop/login1", "org.freedesktop.login1.Manager.Reboot",
                             "boolean:true",
                           };
        execute(dbusSend, args);
        break;
    }
    case Actions::HARD_POWEROFF : {                 // system command
        QStringList args;
        execute("/usr/bin/poweroff", args);
        break;
    }
    case Actions::HARD_REBOOT : {                   // system command
        QStringList args;
        execute("/usr/bin/reboot", args);
        break;
    }
    }
}
