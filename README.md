# Shuttimer

![shuttimer](shuttimer.png)

## Description

`Shuttimer` is a shut down timer for Unix/KDE.

## Screenshots

![shuttimer](shuttimer-1.png)

![shuttimer choices](shuttimer-2.png)

## Features

It allows you to program different types of timed shutdown for your computer from GUI.

    Bell
    Lock session
    Logout
    Suspend
    Hibernate
    Logout and shutdown
    Logout and reboot
    Hard shutdown
    Hard reboot

## Expandable

`Shuttimer` uses `dbus` commands to logout and shutdown/reboot from you session.

It can be easily expanded with other `dbus` commands .

## Command line options

    $ shuttimer --help
    Usage: ./shuttimer [options]
    Shutdown timer.
    
    Options:
      -h, --help             Displays help on commandline options.
      --help-all             Displays help including Qt specific options.
      -v, --version          Displays version information.
      -A, --author           Show author informations.
      -i, --license          Show license informations.
      -d, --delay <delay>    Timer delay: HH:MM:SS.
      -a, --action <action>  Actions: BELL, LOCK, LOGOUT, SUSPEND, HIBERNATE, SESSION_SHUTDOWN, SESSION_REBOOT, POWEROFF, REBOOT.
      -s, --start            Start timer now.
    
## Installation

You can build `shuttimer` from `tar.gz` file: 

    $ qmake && make
    
Install it as `root`:

    $ make install

Contains a `.spec` file to build a Mageia package.

Available with French translation.

## Authors

2023-2024 - Written by Bruno ANSELME  with Qt.

## License

License: [GPL-3.0-or-later](license-gpl-3.0.txt).

Mixkit Sound Effects Free License.

Bell sound: [https://mixkit.co/free-sound-effects/](https://mixkit.co/free-sound-effects/)
