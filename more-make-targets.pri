# ---------------------------------------------------------------
# Additionnal make commands for .pro files
# Bruno ANSELME - <be.root@free.fr>
# Add make targets : translation, files, version, count and doc
# ---------------------------------------------------------------
unix {
  QMAKE_DATE  = $$system_quote($$system(date +"%F\\ %X"))
  ALLFILES    = "$${RESOURCES} $${FORMS} $${SOURCES} $${HEADERS} $${TRANSLATIONS} $${OTHER_FILES}"
  NL          = $$escape_expand(\\n\\t)
  ZIP       = "/bin/tar czf"
  TARNAME   = "$${TARGET}-$${VERSION}"
  ARCHIVDIR = /home/public/Archives/be.root
  ARCHIVDIR = "$$ARCHIVDIR/"
  BUILDDIR   = ~/rpmbuild
  BUILDDIR   = "$$BUILDDIR/"

  # --- make zip ---
  zip.target = zip
  zip.depends = clean version
  zip.commands = \
    mkdir $${TARNAME} $${NL}\
    cp -fr $${ALLFILES} $${TARNAME} $${NL}\
    $$ZIP $${TARNAME}.tar.gz $${TARNAME}/* $${NL}\
    mv $${TARNAME}.tar.gz $$ARCHIVDIR $${NL}\
    rm -Rf $${TARNAME} $${NL}\
    @echo "Archive on " `hostname` ": $$ARCHIVDIR$${TARNAME}.tar.gz"
  QMAKE_EXTRA_TARGETS += zip

  # --- make rpm ---
  #   Create source package in $${BUILDDIR}/SRPMS
  #   Create binary package for host architecture (i586 or x86_64) for Mageia in $${BUILDDIR}/RPMS
  rpm.target = rpm
  rpm.depends = zip
  rpm.commands = \
    cp $$ARCHIVDIR$${TARNAME}.tar.gz $${BUILDDIR}SOURCES/ && \
    rpmbuild -ba --clean --target=$${QMAKE_HOST.arch} $${TARGET}.spec
  QMAKE_EXTRA_TARGETS += rpm

  # --- make translation ---
  translation.target = translation
  translation.depends = .
  translation.commands = \
    lupdate $${TARGET}.pro -no-obsolete 2>/dev/null ; \
    linguist $${TARGET}_fr_FR.ts  ; \
    lrelease $${TARGET}.pro
  QMAKE_EXTRA_TARGETS += translation

  # --- make files ---
  files.target = files
  files.depends = .
  files.commands = @ls -1R $${ALLFILES}
  QMAKE_EXTRA_TARGETS += files

  # --- make version ---
  version.target = version
  version.depends = $${TARGET}.pro
  version.commands = \
    sed -i s/Version:\\ .*/Version:\\ $${VERSION}/ $${TARGET}.spec ; \
    sed -i s/define\\ APP_MAJOR\\ .*/define\\ APP_MAJOR\\ \\ \\ \\ \\ $${MAJOR}/ app_version.h ; \
    sed -i s/define\\ APP_MINOR\\ .*/define\\ APP_MINOR\\ \\ \\ \\ \\ $${MINOR}/ app_version.h ; \
    sed -i s/define\\ APP_RELEASE\\ .*/define\\ APP_RELEASE\\ \\ \\ $${RELEASE}/ app_version.h ; \
    sed -i s/define\\ APP_BUILD\\ .*/define\\ APP_BUILD\\ \\ \\ \\ \\ \\\"$${QMAKE_DATE}\\\"/ app_version.h ; \
    sed -i s/define\\ APP_VERSION\\ .*/define\\ APP_VERSION\\ \\ \\ \\\"$${VERSION}\\\"/ app_version.h
  QMAKE_EXTRA_TARGETS += version

  # --- make count ---
  countlines.target = count
  countlines.depends = clean
  countlines.commands = \
    @echo "Code lines count: " ; wc -l *.h *.cpp
  QMAKE_EXTRA_TARGETS += countlines

  # --- make help ---
  help.target = help
  help.depends = .
  help.commands = \
    @echo -e "Additionnal make targets:\\\n" \
     " zip\\\t\\\t\\\t: Build tar.gz with version number and move it to $$ARCHIVDIR\\\n" \
     " rpm\\\t\\\t\\\t: Build local rpm\\\n" \
     " version\\\t\\\t: Update version number and build date\\\n" \
     " files\\\t\\\t\\\t: List all files to archive\\\n" \
     " count\\\t\\\t\\\t: Count code lines\\\n"
}
