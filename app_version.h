/*
 * SPDX-FileCopyrightText: 2023 Bruno Anselme <be.root@gfree.fr>
 *
 * Authors:
 *   Bruno Anselme <be.root@gfree.fr>
 *
 *   SPDX-License-Identifier: GPL-3.0-or-later
 */
/* app_version.h  -- Global definitions for ShutTimer */
/* APP_MAJOR, APP_MINOR, APP_RELEASE, APP_VERSION and APP_BUILD are updated by "make version" */

#ifndef APP_VERSION_H
#define APP_VERSION_H

#include <QDir>

#define APP_ORG         "BeRoot"
#define APP_NAME        "ShutTimer"
#define APP_STR         "shuttimer"
#define APP_AUTHOR      "Bruno ANSELME"
#define APP_SITE        "http://be.root.free.fr/"
#define APP_MAIL        "be.root@free.fr"

// Don't edit next lines. Defines updated by "make version" from .pro variables.
#define APP_MAJOR     0
#define APP_MINOR     22
#define APP_RELEASE   0
#define APP_VERSION   "0.22.0"
#define APP_BUILD     "2024-01-05 21:28:39"

#endif // APP_VERSION_H
